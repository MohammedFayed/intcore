<?php

function locales()
{

        return \Modules\Languages\Models\Local::select('id','local_name','code','direction')->get();

}


function productOrder($order)
{
    $product =  \Modules\Orders\Models\OrderProduct::where('order_id',$order)->get();

    return $product;
}


function getAccordingLang($text, $lang = null)
{

    if ($lang == null) {
        $lang = app()->getLocale();
    }

    $json_decoded = json_decode($text);

    try {
        return $json_decoded->$lang;
    } catch (Exception $e) {
        return false;
    }
}





