<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([

            \Modules\Admins\Database\Seeders\AdminsDatabaseSeeder::class,
            \Modules\User\Database\Seeders\UserDatabaseSeeder::class,
            \Modules\Categories\Database\Seeders\CategoriesDatabaseSeeder::class,
            \Modules\Languages\Database\Seeders\LocalDatabaseSeeder::class,
            \Modules\Products\Database\Seeders\ProductsDatabaseSeeder::class,
            \Modules\Orders\Database\Seeders\OrdersDatabaseSeeder::class


        ]);
    }
}
