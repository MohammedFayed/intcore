<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Languages\Models\Local;
use Modules\User\Models\User;
use \App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\BaseController;

class UserSiteController extends BaseController
{
    /**
     * Show the specified resource.
     * @return Response
     */
    public function view($locale)
    {
        $this->viewData['user'] = auth()->user();
        return view('site.user_account.settings', $this->viewData);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     * 
     * @todo revise this mess
     */
    public function update(Request $request, $locale)
    {
        $validatedData = $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'date_of_birth' => 'required',
            'mobile' => 'required',
            // 'home_number' => 'required',
            'email' => 'required',
            'password' => 'confirmed',
        ]);

        if (!empty($validatedData['password'])) {
            $validatedData['password'] = Hash::make($validatedData['password']);
        } else {
            unset($validatedData['password']);
        }

        User::where('id', auth()->id())->update($validatedData);

        return redirect(route('user_account.view', [$locale]))->with(['success' => 'Your information has been updated, successfully!']);
    }
}
