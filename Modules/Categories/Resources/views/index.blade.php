@extends('admin.layouts.master')
@section('page-title','Categories')
@section('breadcrumb')
<li class='breadcrumb-item {{ !empty($parent) ? "active" : "" }}'>
    @if(!empty($parent))
    <a href='{{url('admin/categories')}}'>Categories</a>
    @else
    Categories
    @endif
</li>
@if(!empty($parent_id))
@php($parents = \Modules\Categories\Models\Category::getParentsFlat($parent_id))
@endif
@if(!empty($parents) && count($parents))
    @foreach($parents as $id => $name)

    <li class="breadcrumb-item">

        <a href="{{url('admin/categories/'.$id)}}">{{json_decode($name,true)['en']}}</a>
    </li>
    @endforeach
@elseif(!empty($parent))
<li class="breadcrumb-item active">
    {{json_decode($parent->name)->en}}
@endif
@endsection
@section('content')


<div class="card">
    <div class="header">
        @if((!empty($parent) && count($parents) < 4) || empty($parent))
        <a href="{{url('admin/categories/create/'.$parent_id)}}" class="btn btn-primary pull-right">
            <i class="fa fa-plus-square"></i>
            <span>Add New</span>
        </a>
        @endif
        <h2>Categories list</h2>
    </div>
    <div class="body">
        @include('admin.pratical.message')
        <div class="table-responsive">
            <table class="table table-hover m-b-0 c_list datatable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Options</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script>
    $(function () {
        $('.datatable').DataTable({
            processing: true,
            searching: true,
            serverSide: true,
            ajax: "{{ url('admin/categories/datatable/?parent=' . (!empty($parent) ? $parent->id : 0) ) }}",
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'options',
                    name: 'options'
                },
            ]
        });
    });

    function approve(id) {
        $.get("{{ url('dashboard/countries/flag/') }}/" + id,
            function (response) {
                if (response.state == 'deapproved') {
                    $('a[data-id="' + id + '"]').removeClass('btn-danger').addClass('btn-success').text('approve');
                } else {
                    $('a[data-id="' + id + '"]').removeClass('btn-success').addClass('btn-danger').text(
                        'Deapprove');
                }
            });
    }

</script>
@stop
