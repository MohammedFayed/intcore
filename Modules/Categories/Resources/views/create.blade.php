@extends('admin.layouts.master')
@section('page-title','New Category')
@section('breadcrumb')
    <li class="breadcrumb-item {{(empty($parent))?"active":""}}">@if(!empty($parent))<a href='{{url('admin/categories')}}'>Categories</a>@else Categories @endif</li>
    @if(!empty($parent))
        @foreach(\Modules\Categories\Models\Category::getParentsFlat($parent) as $id=>$name)
            <li class="breadcrumb-item {{($parent == $id)?'active':''}}"><a href="{{url('admin/categories/'.$id)}}">{{$name}}</a></li>
        @endforeach
    @endif
        <li class="breadcrumb-item active">Create New</li>
@endsection
@section('content')
    {{Form::open(['action'=>'\Modules\Categories\Http\Controllers\CategoriesController@store'])}}

    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>Create New Category</h2>
            </div>
            <div class="body">
                @include('admin.pratical.message')
                @include('categories::form')
            </div>
        </div>
    </div>




    </div>
    {{Form::close()}}

@endsection
@section('styles')
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('assets/css/color_skins.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery/jquery-sortable.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/css/select2.min.css') }}">
@endsection

