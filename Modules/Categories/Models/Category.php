<?php

namespace Modules\Categories\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Attributes\Models\Attribute;
use Modules\Products\Models\Product;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['parent_id', 'is_active'];
   // protected $translatable = ['name', 'description', 'slug'];
    protected $casts = ['is_active' => 'boolean'];
 //   protected $appends =['en','ar'];


    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    public function parent()
    {
        $this->depth++;
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public static function getChildrenFlat($parent = 0, $maxDepth = 3)
    {
        $items = Category::with('childrenRecursive')->where('parent_id', $parent)->get()->toArray();

        $items = (new self)->recursiveChildrenToFlat($items, '-', 1, $maxDepth);
       // dd($items);
        return $items;
    }

    protected function recursiveChildrenToFlat($array, $dashes = "-", $depth = 1, $maxDepth = 4, $recursive_key = 'children_recursive')
    {
        $results = [];
        if ($depth <= $maxDepth) {
            foreach ($array as $k => $item) {
                $results[$item['id']] = $dashes . ' ' . json_decode($item['name'])->en;
                if (is_array($item[$recursive_key]) && count($item[$recursive_key]) > 0) {
                    $results = $results + $this->recursiveChildrenToFlat($item[$recursive_key], '-' . $dashes, ++$depth, $maxDepth);
                }
            }
        }
     //   dd($results);
        return $results;
    }

    public static function getParentsFlat($parent = 0)
    {
        $items = Category::with('parentRecursive')->where('id', $parent)->first();
        if (!$items) {
            return [];
        }
        $items = $items->toArray();
        $items = (new self)->recursiveParentToFlat($items, 'parent_recursive', 0);
        $items = array_reverse($items, true);
        unset($items[$parent]);
        //array_pop($items);

        return $items;
    }

    protected function recursiveParentToFlat($item, $recursive_key = 'children_recursive', $i)
    {
        if (count($item) == 0) {
            $results = [];
        }

        $results[$item['id']] = $item['name'];
        if (is_array($item[$recursive_key]) && count($item[$recursive_key]) > 0) {
            $i++;
            $results = $results + $this->recursiveParentToFlat($item[$recursive_key], $recursive_key, $i);
        }
        return $results;
    }

    public function toArray()
    {
        $array = parent::toArray();
        $array['name'] = $this->name;
        return $array;
    }
}
