<?php

namespace Modules\Categories\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Categories\Models\Category;

class CategoriesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $category_1 = Category::create([
            'parent_id' => 0,
            'name'=> '{"ar":"التصنيف 1","en":"Category 1"}',
            'tags'=> '{"ar":"التصنيف 1","en":"Category 1"}',
            'description'=> '{"ar":"التصنيف 1","en":"Category 1"}',
            'is_active' => 1,
        ]);


        $category_2 = Category::create([
            'parent_id' => 0,
            'is_active' => 1,
            'name'=> '{"ar":"التصنيف 2","en":"Category 2"}',
            'tags'=> '{"ar":"التصنيف 2","en":"Category 2"}',
            'description'=> '{"ar":"التصنيف 2","en":"Category 2"}',
        ]);


        $subcategory_11 = Category::create([
            'parent_id' => $category_1->id,
            'name'=> '{"ar":"التصنيف 3","en":"Category 3"}',
            'tags'=> '{"ar":"التصنيف 3","en":"Category 3"}',
            'description'=> '{"ar":"التصنيف 3","en":"Category 3"}',
            'is_active' => 1,
        ]);

        $subcategory_12 = Category::create([
            'parent_id' => $category_1->id,
            'name'=> '{"ar":"التصنيف 4","en":"Category 4"}',
            'tags'=> '{"ar":"التصنيف 4","en":"Category 4"}',
            'description'=> '{"ar":"التصنيف 4","en":"Category 4"}',
            'is_active' => 1,
        ]);


        $subcategory_21 = Category::create([
            'parent_id' => $category_2->id,
            'name'=> '{"ar":"التصنيف 5","en":"Category 5"}',
            'tags'=> '{"ar":"التصنيف 5","en":"Category 5"}',
            'description'=> '{"ar":"التصنيف 5","en":"Category 5"}',
            'is_active' => 1,
        ]);

    }
}
