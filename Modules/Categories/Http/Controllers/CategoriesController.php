<?php

namespace Modules\Categories\Http\Controllers;

use Modules\Attributes\Models\Attribute;
use Modules\Languages\Models\Local;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Http\Controllers\Controller;
use Modules\Categories\Models\Category;
use Modules\Languages\Models\Translation;

class CategoriesController extends Controller
{
    protected $results = [];
    protected $i = 0;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($parent_id = 0)
    {
        if ($parent_id) {
            $parent = Category::findOrFail($parent_id);
        }

        return view('categories::index', compact('parent', 'parent_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request, $parent = 0)
    {


        if ($parent)
            Category::findOrFail($request->parent);

        $locales = Local::all();

        $categories = Category::getChildrenFlat(0, 3);


        if ($parent = 0) {
            $depth = count(Category::getParentsFlat($parent));
            if ($depth >= 4) {
                abort(503);
            }
        }

        $parent = $request->parent;
        return view('categories::create', compact('locales', 'categories', 'parent', 'parentAttributes','locales'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'en.name' => 'required',
            'ar.name' => 'required',
            'ar.tags' => 'required',
        ]);




        $category = new Category();

        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'] , 'en' => $request->en['description']],JSON_UNESCAPED_UNICODE);
        $tags = json_encode(['ar' => $request->ar['tags'] , 'en' => $request->en['tags']],JSON_UNESCAPED_UNICODE);



        $category->name        = $name;
        $category->description = $description;
        $category->tags        = $tags;
        $category->parent_id   = $request->parent_id;
        $category->is_active   = $request->is_active ?? 0;
        $category->save();

        return redirect(url('admin/categories/' . $category->parent_id))->with(['success' => 'Category Created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('categories::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Category $category)
    {
        $categories = Category::getChildrenFlat(0, 3);
        $parent = $category->parent_id;
        $name = json_decode($category->name,true);
        $description = json_decode($category->description,true);
        $tags = json_decode($category->tags,true);
        return view('categories::edit', compact('name','category','description','categories', 'parent', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Category $category, Request $request)
    {
        $this->validate($request, [
            'en.name' => 'required',
            'ar.name' => 'required',
            'ar.tags' => 'required',
        ]);

        $name = json_encode(['ar' => $request->ar['name'] , 'en' => $request->en['name']],JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'] , 'en' => $request->en['description']],JSON_UNESCAPED_UNICODE);
        $tags = json_encode(['ar' => $request->ar['tags'] , 'en' => $request->en['tags']],JSON_UNESCAPED_UNICODE);

        $category->name        = $name;
        $category->description = $description;
        $category->tags        = $tags;
        $category->parent_id = $request->parent_id;
        $category->is_active = $request->is_active ?? 0;
        $category->save();

        return redirect(url('admin/categories/' . $category->parent_id))->with(['success' => 'Category Updated Successfully']);
    }


    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(url('admin/categories/' . $category->parent_id))->with(['success' => 'Category Deleted Successfully']);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function datatable(Request $request)
    {
        $categories = Category::whereParentId($request->parent ?? 0)->get();
        return \DataTables::of($categories)
            ->editColumn('name', function ($model) {
                $enName = json_decode($model->name,true);
                return "<a href='" . url('admin/categories/' . $model->id) . "'>" .$enName['en']. "</a>";
            })->editColumn('options', function ($model) {
                return "<a href='" . url('admin/categories/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/categories/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->rawColumns(['options','name'])

            ->make(true);
    }

}
