<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\ImportToolLogViewer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attributes\Models\Attribute;
use Modules\Branches\Models\Branch;
use Modules\Categories\Models\Category;
use Modules\Classifications\Models\Classification;
use Modules\Identifiers\Models\Identifier;
use Modules\Products\Models\Product;
use Modules\Products\Models\ProductImage;
use Modules\Units\Models\Unit;
use DB;
use File;
use Modules\Attributes\Models\AttributeProduct;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
class ProductsController extends Controller
{

    protected $list = [];
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('products::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {


        $categories = Category::getChildrenFlat();

        return view('products::create', compact( 'categories'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'en.name' => 'required',
            'ar.name' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'discount_price' => 'required',
            'featured_image' => 'required|image',
        ]);

        $product = new Product();

        $name = json_encode(['ar' => $request->ar['name'], 'en' => $request->en['name']], JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'], 'en' => $request->en['description']], JSON_UNESCAPED_UNICODE);
        $tags = json_encode(['ar' => $request->ar['tags'], 'en' => $request->en['tags']], JSON_UNESCAPED_UNICODE);

        $product->name = $name;
        $product->description = $description;
        $product->tags = $tags;
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->discount_price = $request->discount_price;
        $product->featured_image = $request->featured_image->store('products');
        $product->save();
       // $product->translateAttributes($request->only('ar', 'en'));
        if ($request->filled('uploadedFiles')) {
            $files = json_decode($request->get('uploadedFiles'));
            ProductImage::whereIn('id', $files)->update(['product_id' => $product->id]);
        }


        return redirect('admin/products')->with(['success' => 'product created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        // return view('products::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $category_id = $product->category_id;

        $name = json_decode($product->name, true);
        $description = json_decode($product->description, true);
        $tags = json_decode($product->tags, true);


        $categories = Category::getChildrenFlat();


        return view('products::edit', compact( 'category_id', 'categories', 'name', 'description', 'tags', 'product'));
    }


    /**
     * @param Request $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'en.name' => 'required',
            'ar.name' => 'required',
            'category_id' => 'required',
            'price' => 'required',
            'discount_price' => 'required',
            'featured_image' => 'image',
        ]);


        $name = json_encode(['ar' => $request->ar['name'], 'en' => $request->en['name']], JSON_UNESCAPED_UNICODE);
        $description = json_encode(['ar' => $request->ar['description'], 'en' => $request->en['description']], JSON_UNESCAPED_UNICODE);
        $tags = json_encode(['ar' => $request->ar['tags'], 'en' => $request->en['tags']], JSON_UNESCAPED_UNICODE);

        $product->name = $name;
        $product->description = $description;
        $product->tags = $tags;

        $product->category_id = $request->category_id;

        $product->price = $request->price;
        $product->discount_price = $request->discount_price;
        $product->featured_image = ($request->hasFile('featured_image')) ? $request->featured_image->store('products') : $product->featured_image;
        $product->save();

        return redirect('admin/products')->with(['success' => 'product updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('admin/products')->with(['success' => 'product deleted successfully']);

    }

    public function uploadImages(Request $request)
    {
        $this->validate($request, [
            'images' => 'required',
            'images.*' => 'image',
        ]);
        $res = [];
        foreach ($request->images as $k => $image) {
            $img = new ProductImage();
            $img->image = $image->store('products');
            $img->product_id = null;
            $img->save();
            $res = $img->id;
        }
        return response()->json($res);
    }

    public function deleteImage(Request $request)
    {
        ProductImage::where('image', $request->key)->delete();
        return response()->json(['success' => 'true']);
    }

    public function datatable()
    {
        $locations = Product::query();
        return \DataTables::eloquent($locations)
            ->addColumn('name', function ($model) {
                $name = json_decode($model->name, true);
                return $name['en'];
            })->editColumn('options', function ($model) {
                return "<a href='" . url('admin/products/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>
                <a href='" . url('admin/products/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->rawColumns(['options'])

            ->make(true);
    }



    public function chechRecusive($father)
    {
        if ($father->parentRecursive === null) {
            return $this->list;

        } else {
            $this->list[] = $father->parentRecursive->id;
            $this->chechRecusive($father->parentRecursive);
        }
    }












}
