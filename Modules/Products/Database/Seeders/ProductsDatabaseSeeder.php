<?php

namespace Modules\Products\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\ProductImage;
use Modules\Products\Models\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Modules\Categories\Models\Category;
use Modules\Products\Models\BranchProduct;
use Modules\Branches\Models\Branch;
use Modules\Attributes\Models\Attribute;

class ProductsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Storage::persistentFake("products");

        $category_1 = Category::find(1);
        for ($i = 1; $i <= 5; $i++) {
            $product = Product::create([
                'name' => '{"ar":"المنتج' . $i . '","en":"Product' . $i . '"}',
                'description' => '{"ar":"وصف المنتج  ' . $i . '","en":"description' . $i . '"}',
                'category_id' => $category_1->id,
                'price' => 1000,
                'discount_price' => 900,
                'featured_image' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('products', 'public'),
            ]);


            for ($x = 1; $x <= 2; $x++) {
                ProductImage::create([
                    'image' => UploadedFile::fake()->image("picture_{$product->id}_{$x}.jpg", 400, 400)->store('products', 'public'),
                    'product_id' => $product->id,
                ]);
            }

        }

        $category_2 = Category::find(2);
        for ($i = 6; $i <= 7; $i++) {
            $product = Product::create([
                'name' => '{"ar":"المنتج' . $i . '","en":"Product' . $i . '"}',
                'description' => '{"ar":"وصف المنتج  ' . $i . '","en":"description' . $i . '"}',
                'category_id' => $category_2->id,
                'price' => 1000,
                'discount_price' => 900,
                'featured_image' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('products', 'public'),
            ]);


        }

    }
}
