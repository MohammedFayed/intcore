@extends('admin.layouts.master')
@section('page-title','Create Product')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/products')}}">Products</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
{{Form::open(['route'=>'products.store','files'=>true])}}

    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Create New Product </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('admin.pratical.message')
                    <div class="table-responsive">
                        <ul class="nav nav-tabs">
                            @foreach(locales() as $k=>$local)
                                <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach(locales() as $k=>$local)
                                <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name {{ $local->code}} </label>
                                                {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description {{ $local->code}} </label>
                                                {{Form::textarea($local->code.'[description]',null,['class'=>'form-control','rows'=>3])}}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>Tags {{ $local->code}} </label>

                                            <div class="input-group demo-tagsinput-area">
                                                {{Form::text($local->code.'[tags]',null,['class'=>'form-control','data-role'=>'tagsinput'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach




                        </div>
                    </div>
                </div>
            </div>




            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Images </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Featured Image</label>
                        {{Form::file('featured_image',['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Images</label>
                        <div class="row">
                            {{Form::hidden('uploadedFiles')}}
                            <div class="col-md-12">
                                <div class="file-loading">
                                    <input id="input-pr" name="images[]" type="file" data-allowed-file-extensions='["png", "jpg","jpeg"]'  accept="image/*"  multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>


        <div class="col-md-3">

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Pricing </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Price</label>
                        {{Form::number('price',null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Discount Price</label>
                        {{Form::number('discount_price',null,['class'=>'form-control'])}}
                    </div>

                    <div class="form-group">
                        <label>Category</label>
                        {{Form::select('category_id',$categories,$categories[1],['class'=>'form-control','id'=>'category'])}}
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Save </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block"> Save </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-block"> Cancel </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

{{Form::close()}}


@endsection
@section('styles')
    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
@endsection

@section('scripts')
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/piexif.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/sortable.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/purify.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/fileinput.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>

    <script>


function handleCategoryChange() {
    $( "#att" ).html("");
    $( "#test" ).html("");

}
            // var urls =[
            //     'http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg',
            //     'http://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Earth_Eastern_Hemisphere.jpg/600px-Earth_Eastern_Hemisphere.jpg'
            // ];
            var uploadedIDs = [];
            $("#input-pr").fileinput({
                uploadUrl: "{{route('product.images.upload')}}",
                uploadAsync: true,
                maxFileCount: 10,
                overwriteInitial: false,
                // initialPreview: urls,
                // initialPreviewAsData: true, // allows you to set a raw markup
                // initialPreviewFileType: 'image', // image is the default and can be overridden in config below
                // initialPreviewDownloadUrl: 'https://picsum.photos/800/460?image={key}', // includes the dynamic key tag to be replaced for each config
                // initialPreviewConfig: [
                //     {type: "image", caption: "Image-1.jpg", size: 847000, key: 1},
                //     {type: "image", caption: "Image-2.jpg", size: 817000, key: 2},  // set as raw markup
                // ],
                deleteUrl: "{{route('product.images.delete')}}",
                uploadExtraData: {
                    _token: "{{csrf_token()}}",
                },
                deleteExtraData: {
                    _token: "{{csrf_token()}}"
                }
            }).on('fileuploaded', function(e, params) {
                uploadedIDs.push(params.jqXHR.responseJSON);
                jsonFiles = JSON.stringify(uploadedIDs);
                $('input[name="uploadedFiles"]').val(jsonFiles);
            });








    </script>


@endsection


