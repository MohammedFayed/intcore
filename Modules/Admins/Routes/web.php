<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('admin:admin')->group(function() {

    Route::get('admins/{admin}/delete', 'AdminsController@destroy');
    Route::get('admins/datatable', 'AdminsController@datatable')->name('admins.datatable');
    Route::resource('admins', 'AdminsController');

});
