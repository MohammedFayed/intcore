@extends('admin.layouts.master')
@section('page-title','Create Order')

@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/orders')}}">Orders</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h2>Order Details</h2>

                </div>
                <div class="body">
                    {{--<form id="wizard_with_validation" method="POST">--}}
                    {{Form::open(['route'=>'orders.store','files'=>true , 'id'=>'wizard_with_validation'])}}

                    <h3>Main Information</h3>
                    <fieldset>


                        <div class="form-group form-float">
                            {{--<input type="password" class="form-control" placeholder="Confirm Password *" name="confirm" required>--}}
                            <label>User</label>
                            <select class="form-control disease" name="user_id" id="user" placeholder="Choose User .."
                                    required>
                                <option value="new">create new user</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="clearfix"></div>
                        <a href="{{ url('admin/user/create') }}" target="_blank" id="button"
                           style="display:none;background-color: #007bff;color: white;padding: 14px 25px;text-align: center;text-decoration: none;"><i
                                    class="fa fa-plus-square"></i>create new user</a>

                        <div class="clearfix"></div>



                    </fieldset>
                    <h3>Product Information</h3>
                    <fieldset>
                        <div class="row fieldGroup">

                            <div class="col-md-5">
                                <div class="form-group form-float">
                                    <label>Product</label><br>
                                    {{Form::select('product_id[]',$selected_product,null,['class'=>'form-control disease product','style'=>'width:100%','required'=>'required','placeholder'=>'choose Product ..'])}}
                                </div>
                            </div>


                            <div class="col-md-5">
                                <div class="form-group form-float">
                                    <label>Quantity</label>
                                    {{Form::number('quantity[]',null,['class'=>'form-control','required'=>'required','style'=>'width:100%',])}}
                                </div>
                            </div>



                            <div class="form-group ">
                                <label></label>
                                <button type="button" class="btn btn-primary btn-labeled addMoreAttribute">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>

                        </div>
                    </fieldset>

                    {{Form::close()}}


                    <div class="row fieldGroupCopy" style="display: none;">

                        <div class="col-md-5">
                            <div class="form-group form-float">
                                <label>Product</label><br>
                                {{Form::select('product_id[]',$selected_product,null,['class'=>'form-control test product','id'=>'product','style'=>'width:100%','required'=>'required','placeholder'=>'choose Product ..'])}}
                            </div>
                        </div>


                        <div class="col-md-5">
                            <div class="form-group form-float">
                                <label>Quantity</label>
                                {{Form::number('quantity[]',null,['class'=>'form-control','required'=>'required','style'=>'width:100%',])}}
                            </div>
                        </div>

                        <div class="form-group ">
                            <label></label>
                            <button type="button" class="btn btn-primary btn-labeled remove">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection



@section('styles')

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet"
          type="text/css"/>
    {{--<link rel="stylesheet" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">--}}


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">


@endsection


@section('scripts')


    {{--<script src="{{ asset('assets/vendor/daterangepicker/moment.min.js')}}"></script>--}}
    {{--<script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>--}}
    {{--<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>--}}
    {{--<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>--}}


    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>


    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>

    <script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script> <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/vendor/jquery-steps/jquery.steps.js')}}"></script> <!-- JQuery Steps Plugin Js -->

    <script src="{{ asset('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{ asset('assets/js/pages/forms/form-wizard.js')}}"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>




    <script>
        $(document).ready(function () {

            $(function () {

                $('#datetimepicker1').datetimepicker();
            });


            $('input:radio[name="type"]').change(function () {
                if ($(this).is(':checked') && $(this).val() == '1') {

                    $('#dateTime').show();

                    $('#dateTime').datetimepicker({
                        minDate: new Date(),
                        timePicker: true,
                        locale: {
                            format: 'dd/MM/yyyy hh:mm:ss',
                        }
                    });
                } else {
                    $('#dateTime').hide();

                }
            });

        });
    </script>

    <script>








    </script>


    <script>
        $(document).ready(function () {
            // $('.disease').select2();
            $('.disease').select2();


            $("#user").change(function () {
                var value = $(this).val();
                if (value === 'new') {
                    $("#button").show();

                } else {
                    $("#button").hide();
                }

            });

        });




    </script>





    <script>
        $(document).ready(function () {
            //group add limit
            var maxGroup = 20;
            $('.test').select2();
            //add more fields group
            $(".addMoreAttribute").click(function () {
                $('.test').select2("destroy");
                if ($('body').find('.fieldGroup').length < maxGroup) {
                    var fieldHTML = '<div class="row fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                    $('.test').select2();


                } else {
                    alert('Maximum ' + maxGroup + ' groups are allowed.');
                }
            });

            //remove fields group
            $("body").on("click", ".remove", function () {
                $(this).parents(".fieldGroup").remove();
            });


        });

    </script>

@endsection







