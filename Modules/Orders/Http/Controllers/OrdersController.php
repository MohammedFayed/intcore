<?php

namespace Modules\Orders\Http\Controllers;

use App\Mail\AcceptOrder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Branches\Models\Branch;
use Modules\Coupons\Models\Coupon;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\Locations\Models\Location;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\BranchProduct;
use Modules\Orders\Models\OrderProduct;
use Modules\Orders\Models\OrderStatus;
use Modules\Products\Models\Product;
use Modules\User\Models\User;
use Session;
use DB;
use Modules\DeliveryLocation\Models\UserAddress;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {


        $processing = OrderStatus::where('status', 1)->get()->count();
        $delivering = OrderStatus::where('status', 2)->get()->count();
        $delivered = OrderStatus::where('status', 3)->get()->count();
        $canceled = OrderStatus::where('status', 4)->get()->count();
        $pending = OrderStatus::where('status', 5)->get()->count();
        return view('orders::index', compact('processing', 'delivered', 'delivering', 'canceled', 'pending'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {


        $poducts = Product::pluck('name', 'id')->toArray();
        foreach ( $poducts as $key => $value){
            $selected_product[$key] = getAccordingLang($value , app()->getLocale());
        }

        $users = User::get();
        $status = OrderStatus::status();
        return view('orders::create', compact('selected_product', 'users', 'poducts', 'status'));
    }



    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'user_id' => 'required',
        ]);

        $order = new Order();
        $order->user_id = $request->user_id;
        $order->save();
        $order->status()->create(['status' => 5]);


        $data = [];
        foreach ($request->product_id as $k => $t) {
            $product = Product::where('id', $t)->first();
            $data[$t] = ['price' => (int)$product->price * $request->quantity[$k], 'discount_price' => (int)$product->discount_price * $request->quantity[$k], 'quantity' => $request->quantity[$k]];
        }


        $order->products()->sync($data);

        return redirect('admin/orders')->with(['success' => 'order created successfully']);
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Order $order)
    {
        $product = productOrder($order->id);



        $poducts = Product::pluck('name', 'id')->toArray();
        foreach ( $poducts as $key => $value){
            $selected_product[$key] = getAccordingLang($value , app()->getLocale());
        }



        $users = User::get();
        $status = OrderStatus::status();

        return view('orders::edit', compact( 'poducts','selected_product', 'users', 'order', 'status', 'product'));
    }


    public function shows($id)
    {
        $order = Order::with(['user'])->find($id);
        $orderss = Order::find($id);

        $user = User::find($order->user_id);
        $countOrder = Order::where('user_id', $order->user_id)->count();
        $orderUser = Order::where('user_id', $order->user_id)->orderBy('id', 'desc')->take(5)->get()->toArray();
        $productCount = OrderProduct::whereIn('order_id', $orderUser)->count();
        $orderProducts = Order::productData($id);
        $price = Order::productAfterDiscountTotal($id);
        $subTotal = Order::productBeforeDiscountTotal($id);
        $total = Order::totalPrice($id);

        return view('orders::show', compact('order', 'address','orderss', 'countOrder', 'total', 'productCount', 'orderUser', 'orderProducts', 'price', 'user', 'subTotal'));
    }




    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',

        ]);
        $order = Order::find($id);


        $order->user_id = $request->user_id;
        $order->update();

        if ($request->status != null) {
            $order->status()->delete();
            $order->status()->create(['status' => $request->status]);
        }

        $data = [];
        foreach ($request->product_id as $k => $t) {
            $product = Product::where('id', $t)->first();
            $data[$t] = ['price' => (int)$product->price * $request->quantity[$k], 'discount_price' => (int)$product->discount_price * $request->quantity[$k], 'quantity' => $request->quantity[$k]];
        }


        $order->products()->detach();

        $order->products()->attach($data);

        return redirect('admin/orders')->with(['success' => 'order Updated successfully']);


    }


    /**
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        // $order->deleteTranslations();
        return redirect('admin/orders')->with(['success' => 'product deleted successfully']);
    }


    public function productPrice($id)
    {
        $product = Product::find($id);
        return $product;

    }





    public function datatable()
    {
        $orders = Order::with('products', 'status')->orderBy('created_at', 'Desc')->get();

        return \DataTables::of($orders)
            ->editColumn('status', function ($model) {
                $status = OrderStatus::where('order_id', $model->id)->first();
                if (!empty($status)) {
                    if ($status->status == 1) {
                        return '<span class="badge badge-success">Processing</span>';
                    } elseif ($status->status == 2) {
                        return '<span class="badge badge-warning">Delivering</span>';
                    } elseif ($status->status == 3) {
                        return '<span class="badge badge-danger">Delivered</span>';
                    } elseif ($status->status == 4) {
                        return '<span class="badge badge-default">Canceled</span>';
                    } elseif ($status->status == 5) {
                        return '<span class="badge badge-info">Pending</span>';
                    } else {
                        return '';
                    }
                }
                return 'null';
            })

            ->editColumn('user', function ($model) {
                $user = User::where('id', $model->user_id)->first();

                return $user->first_name . ' ' . $user->last_name;

            })

            ->editColumn('price', function ($model) {
                $price = Order::totalPrice($model->id);
                return $price;
            })

            ->addColumn('options', function ($model) {
                return "<a href='" . url('admin/orders/' . $model->id . '/show') . "' class='btn btn-info'>Show</a>"
                    . "<a href='" . url('admin/orders/' . $model->id . '/edit') . "' class='btn btn-warning'>Edit</a>"
                    . "<a href='" . url('admin/orders/' . $model->id . '/delete') . "' class='btn btn-danger'>Delete</a>";

            })
            ->rawColumns(['options','status'])
            ->make(true);
    }


    public function getCount(Request $request)
    {
        Order::setSession();
        return $value = Session::get('orderIdCount');
    }

    public function countOrderDatabase()
    {
        $value = Order::count();
        return $value;
    }



}
